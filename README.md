Réalisation de l'algorithme de Dijkstra en C++.
Ce programme peut bien sûr être amélioré : il suppose par exemple qu'il n'existe pas de sommets isolés, ou que l'utilisateur est intelligent et entre bien un entier lorsqu'on lui demande.

By Aldric L. 2020